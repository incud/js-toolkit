package it.univr.js.graph;

import org.graphstream.ui.view.Viewer;
import org.graphstream.ui.view.ViewerListener;
import org.graphstream.ui.view.ViewerPipe;

public class GraphWindow implements ViewerListener {

    static {
        // When the program starts, it sets the properties
        // you need to visualize the incud.graph correctly
        System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
    }

    protected boolean loop = true;
    private Graph graph;
    private Viewer viewer;

    public GraphWindow(Graph graph) {
        // We do as usual to display a incud.graph. This
        // connect the incud.graph outputs to the viewer.
        // The viewer is a sink of the incud.graph.
        //Graph incud.graph = new SingleGraph("Clicks");
        viewer = graph.display();

        this.graph = graph;

        // The default action when closing the view is to quit
        // the program.
        viewer.setCloseFramePolicy(Viewer.CloseFramePolicy.EXIT);

        // We connect back the viewer to the incud.graph,
        // the incud.graph becomes a sink for the viewer.
        // We also install us as a viewer listener to
        // intercept the graphic events.
        ViewerPipe fromViewer = viewer.newViewerPipe();
        fromViewer.addViewerListener(this);
        fromViewer.addSink(graph);

        // Then we need a loop to do our work and to wait for events.
        // In this loop we will need to call the
        // pump() method before each use of the incud.graph to copy back events
        // that have already occurred in the viewer thread inside
        // our thread.

        while(loop) {
            fromViewer.pump(); // or fromViewer.blockingPump(); in the nightly builds

            // here your simulation code.

            // You do not necessarily need to use a loop, this is only an example.
            // as long as you call pump() before using the incud.graph. pump() is non
            // blocking.  If you only use the loop to look at event, use blockingPump()
            // to avoid 100% CPU usage. The blockingPump() method is only available from
            // the nightly builds.
        }
    }

    @Override
    public void viewClosed(String s) {

    }

    @Override
    public void buttonPushed(String s) {

    }

    @Override
    public void buttonReleased(String nodeId) {
        graph.setFrozen(nodeId, true);
    }
}
