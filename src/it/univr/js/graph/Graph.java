package it.univr.js.graph;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;

import java.awt.*;
import java.io.File;
import java.net.MalformedURLException;

/**
 * Create a graph based on the Graphstream's library
 * It automatically loads the properties you need to visualize it correctly
 */
public class Graph extends SingleGraph {

    private static final String CSS_PATH = "src/it/univr/js/graph/style.css";

    /**
     * Node property "label"
     */
    private static final String ATTRIBUTE_LABEL = "ui.label";

    /**
     * Node property "color"
     */
    private static final String ATTRIBUTE_COLOR = "ui.color";

    public Graph() {
        super("Control flow incud.graph");
        setStrict(false); // what does setStrict means?
        loadCss();
    }

    private void loadCss() {
        File myFile = new File(CSS_PATH);
        java.net.URL myUrl = null;
        try {
            myUrl = myFile.toURI().toURL();
            addAttribute("ui.stylesheet", "url('" + myUrl + "')");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Add a new node
     * @param id unique identifier of the node inside the incud.graph
     * @param label label of the node
     */
    public void addNode(String id, String label) {
        Node node = addNode(id);
        node.addAttribute(ATTRIBUTE_LABEL, label);
    }

    /**
     * Add a node and associate a color to it
     * @param id unique identifier of the node inside the incud.graph
     * @param label label of the node
     * @param color background color
     */
    public void addNode(String id, String label, Color color) {
        Node node = addNode(id);
        node.addAttribute(ATTRIBUTE_LABEL, label);
        node.addAttribute(ATTRIBUTE_COLOR, color);
    }

    /**
     * Add a directed edge from the former to the latter node
     * @param from id of the former node
     * @param to id of the latter node
     * @param <T> generic parameter
     * @return edge
     */
    public <T extends Edge> T addEdge(String from, String to) {
        return super.addEdge(from + "<->" + to, from, to, true);
    }

    public <T extends Edge> T addEdge(String from, String to, Color color) {
        T edge = addEdge(from, to);
        edge.setAttribute(ATTRIBUTE_COLOR, color);
        return edge;
    }

    public <T extends Edge> T addEdge(String from, String to, String label) {
        T edge = addEdge(from, to);
        edge.setAttribute(ATTRIBUTE_LABEL, label);
        return edge;
    }

    public <T extends Edge> T addEdge(String from, String to, Color color, String label) {
        T edge = addEdge(from, to);
        edge.setAttribute(ATTRIBUTE_COLOR, color);
        edge.setAttribute(ATTRIBUTE_LABEL, label);
        return edge;
    }

    public <T extends Edge> T addEdge(String from, String to, Color color, String label, double length) {
        T edge = addEdge(from, to);
        edge.setAttribute(ATTRIBUTE_COLOR, color);
        edge.setAttribute(ATTRIBUTE_LABEL, label);
        edge.setAttribute("layout.weight", length);
        return edge;
    }

    /**
     * The wanted node won't float anymore
     * @param id node unique identifier
     * @param setFrozen true if you want the node to be frosen in current position, false otherwise
     */
    public void setFrozen(String id, boolean setFrozen) {
        if(setFrozen) {
            getNode(id).addAttribute("layout.frozen");
        } else {
            getNode(id).removeAttribute("layout.frozen");
        }
    }
}
