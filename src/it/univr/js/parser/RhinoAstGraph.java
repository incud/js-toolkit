package it.univr.js.parser;

import it.univr.js.graph.Graph;
import it.univr.js.parser.RhinoAst;
import org.mozilla.javascript.Node;
import org.mozilla.javascript.Token;
import org.mozilla.javascript.ast.*;

import java.awt.*;

/**
 * Please explain ID assignment
 */
public class RhinoAstGraph extends Graph {

    public RhinoAstGraph(RhinoAst ast) {
        processList(ast, "");
    }

    private boolean isNonNestedNode(String id) {
        // the 'top level' nodes has id in the form :<number>
        // we remove the first character and check if in the remaining part
        // of the id there is some other : characters
        return id.substring(1, id.length()-1).contains(":");
    }

    private void addPrettyPrintedNode(String id, String type, String label) {
        if(!label.isEmpty()) {
            if(!isNonNestedNode(id))
                addNode(id, String.format("(%s %s)", type, label), Color.ORANGE);
            else
                addNode(id, String.format("(%s %s)", type, label));
        } else {
            if(!isNonNestedNode(id))
                addNode(id, String.format("(%s)", type), Color.ORANGE);
            else
                addNode(id, String.format("(%s)", type));
        }
    }

    private void processList(Iterable<? extends Node> nodes, String firstId) {
        String previous = firstId;
        int i = 0;
        for(Node node : nodes) {
            String current = firstId + ":" + (i++);
            processNode((AstNode)node, current);
            addEdge(previous, current);
            previous = current;
        }
    }

    private void processNode(AstNode node, String id) {

        switch (node.getType()) {
            case Token.FUNCTION: {
                processFunctionDefinition((FunctionNode)node, id);
                return;
            }
            case Token.CALL: {
                processFunctionCall((FunctionCall)node, id);
                return;
            }
            case Token.BLOCK: {
                if(node instanceof Block)
                    processBlockNode((Block)node, id);
                else
                    processScopeNode((Scope)node, id);
                return;
            }
            case Token.TRY: {
                processTry((TryStatement)node, id);
                return;
            }
            case Token.CATCH: {
                processCatch((CatchClause)node, id);
                return;
            }
            case Token.FINALLY: {
                // deal with finally block in 'try' block
                throw new AssertionError("Deal with finally case in try case");
            }
            case Token.WHILE: {
                processWhileNode((WhileLoop)node, id);
                return;
            }
            case Token.VAR:
            case Token.CONST:
            case Token.LET: {
                processVariable(node, id);
                return;
            }
            case Token.NAME: {
                addPrettyPrintedNode(id, node.shortName(), ((Name)node).getIdentifier());
                return;
            }
            case Token.EXPR_RESULT: {
                processExpressionResult((ExpressionStatement)node, id);
                return;
            }
            case Token.ASSIGN: {
                processAssign((Assignment)node, id);
                return;
            }
            default: {
                addPrettyPrintedNode(id, node.shortName(), node.toSource());
                return;
            }
        }
    }

    private void processFunctionDefinition(FunctionNode fNode, String id) {
        // add this node
        addPrettyPrintedNode(id, fNode.shortName(), fNode.getFunctionName().getIdentifier());
        // add parameters
        processList(fNode.getParams(), id);
        // add body
        processNode(fNode.getBody(), id + ":BODY");
        addEdge(id, id + ":BODY");
    }

    private void processFunctionCall(FunctionCall fNode, String id) {
        addPrettyPrintedNode(id, fNode.shortName(), fNode.getTarget().toSource());
        // add parameters
        processList(fNode.getArguments(), id);
    }

    private void processBlockNode(Block block, String id) {
        // add this node
        addPrettyPrintedNode(id, block.shortName(), "");
        // add instructions
        processList(block, id);
    }

    private void processScopeNode(Scope scope, String id) {
        // add this node
        addPrettyPrintedNode(id, scope.shortName(), "");
        // add instructions
        processList(scope.getStatements(), id);
    }

    private void processVariable(AstNode node, String id) {

        if(node instanceof VariableDeclaration) {
            VariableDeclaration vd = (VariableDeclaration)node;
            // add this node
            addPrettyPrintedNode(id, vd.shortName(), "");
            // process list
            processList(vd.getVariables(), id);

        } else if(node instanceof VariableInitializer) {
            VariableInitializer vi = (VariableInitializer)node;
            // add this node
            addPrettyPrintedNode(id, vi.shortName(), vi.toSource());
            // add body
            processNode(vi.getTarget(), id + ":TARGET");
            addEdge(id, id + ":TARGET");
            // add initialization, if there is someone
            if(vi.getInitializer() != null) {
                processNode(vi.getInitializer(), id + ":INIT");
                addEdge(id, id + ":INIT");
            }
        }
    }

    private void processTry(TryStatement tryStatement, String id) {
        // add this node
        addPrettyPrintedNode(id, tryStatement.shortName(), "");
        // add try block
        processNode(tryStatement.getTryBlock(), id + ":TRYBLOCK");
        addEdge(id, id + ":TRYBLOCK");
        // add catches
        processList(tryStatement.getCatchClauses(), id);
        // add finally
        if(tryStatement.getFinallyBlock() != null) {
            processNode(tryStatement.getFinallyBlock(), id + ":FINALLY");
            addEdge(id, id + ":FINALLY");
        }
    }

    private void processCatch(CatchClause catchClause, String id) {
        // add this node
        addPrettyPrintedNode(id, catchClause.shortName(), "" + catchClause.getVarName().toSource());
        // add condition
        if(catchClause.getCatchCondition() != null) {
            processNode(catchClause.getCatchCondition(), id + ":CATCHCOND");
            addEdge(id, id + ":CATCHCOND");
        }
        // add body
        processNode(catchClause.getBody(), id + ":CATCHBODY");
        addEdge(id, id + ":CATCHBODY");
    }

    private void processWhileNode(WhileLoop whileLoop, String id) {
        // add this node
        addPrettyPrintedNode(id, whileLoop.shortName(), whileLoop.getCondition().toSource());
        // add condition
        processNode(whileLoop.getCondition(), id + ":COND");
        addEdge(id, id + ":COND");
        // add body
        processNode(whileLoop.getBody(), id + ":BODY");
        addEdge(id, id + ":BODY");
    }

    private void processExpressionResult(ExpressionStatement assignment, String id) {
        // add this node
        addPrettyPrintedNode(id, assignment.shortName(), assignment.toSource());
        // add expression
        processNode(assignment.getExpression(), id + ":EXPR");
        addEdge(id, id + ":EXPR");
    }

    private void processAssign(Assignment assignment, String id) {
        // add this node
        addPrettyPrintedNode(id, assignment.shortName(), assignment.toSource());
        // add left
        processNode(assignment.getLeft(), id + ":LEFT");
        addEdge(id, id + ":LEFT");
        // add right
        processNode(assignment.getRight(), id + ":RIGHT");
        addEdge(id, id + ":RIGHT");
    }
}
