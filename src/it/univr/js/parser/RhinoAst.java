package it.univr.js.parser;
import org.mozilla.javascript.CompilerEnvirons;
import org.mozilla.javascript.IRFactory;
import org.mozilla.javascript.ast.*;

import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

public class RhinoAst implements Iterable<AstNode> {

    private final AstRoot root;

    public RhinoAst(String sourceCode) {
        this.root = parseSourceCode(sourceCode);
    }

    public RhinoAst(FileReader reader) throws IOException {
        this.root = parseFile(reader);
    }

    private AstRoot parseSourceCode(String sourceCode) {
        // Create compiler environment
        CompilerEnvirons env = new CompilerEnvirons();
        env.setRecoverFromErrors(true);

        // Parse
        AstRoot root = (new IRFactory(env)).parse(sourceCode, null, 0);

        return root;
    }

    private AstRoot parseFile(FileReader reader) throws IOException {
        // Create compiler environment
        CompilerEnvirons env = new CompilerEnvirons();
        env.setRecoverFromErrors(true);

        // Parse
        AstRoot root = (new IRFactory(env)).parse(reader, null, 0);

        return root;
    }

    @Override
    public Iterator<AstNode> iterator() {
        return root.getStatements().iterator();
    }

}
