package it.univr.js.builder;

import it.univr.js.builder.abstractpoint.AbstractPoint;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Tag interface that represent a node inside the control flow graph
 * You can access each node by the AbstractPoint which is the point before
 * the execution of this instruction in the control flow graph
 */
public abstract class Statement {

    public Statement() {
        uniqueId = uniqueCounter.incrementAndGet();
    }

    /**
     * Each statement has an unique identifier. This is not used during the analysis,
     * but it's convenient to have it during the visualization of the AST:
     * the external graph visualization library wants each node af the graph to have
     * an unique name
     */

    private static AtomicLong uniqueCounter = new AtomicLong(0);

    private final long uniqueId;

    public long getUniqueNumber() {
        return uniqueId;
    }

    public String getUniqueIdentifier() { return "" + uniqueId; }

    /**
     * Each statement has an AbstractPoint associated with it. Even if there is
     * a setter, you should not use it after the Labeling phase of the analysis
     */

    private AbstractPoint abstractPoint;

    public AbstractPoint getAbstractPoint() {
        return abstractPoint;
    }

    public void setAbstractPoint(AbstractPoint abstractPoint) {
        this.abstractPoint = abstractPoint;
    }

    /**
     * Each statement also have a parent Block. Should it be specified in the constructor
     * and mark the field as immutable? Probably yes (still need to think about that)
     */

    private Block parent;

    public Block getParent() {
        return parent;
    }

    public void setParent(Block parent) {
        this.parent = parent;
    }
}
