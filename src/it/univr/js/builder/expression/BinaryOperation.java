package it.univr.js.builder.expression;

public enum BinaryOperation {

    ADD("+"), SUB("-"), MUL("*"), DIV("/"), MOD("%"),
    AND("and"), OR("or"), XOR("xor"),
    LEFT_SHIFT("<<"), RIGHT_SHIFT(">>"), UNSIGNED_RIGHT_SHIFT(">>>"),
    EQUALS("=="), NOT_EQUALS("!="), EQUALS_REF("==="), NOT_EQUALS_REF("!=="),
    LESS("<"), LESS_EQUALS("<="), GREATER(">"), GREATER_EQUALS(">=");

    private final String operatorSyntax;

    BinaryOperation(String s) {
        this.operatorSyntax = s;
    }

    @Override public String toString() {
        return operatorSyntax;
    }
}
