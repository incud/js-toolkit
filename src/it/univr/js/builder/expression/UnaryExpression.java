package it.univr.js.builder.expression;

public class UnaryExpression implements Expression {

    private final Expression expression;

    private final UnaryOperation operation;

    public UnaryExpression(Expression expression, UnaryOperation operation) {
        this.expression = expression;
        this.operation = operation;
    }

    public Expression getExpression() {
        return expression;
    }

    public UnaryOperation getOperation() {
        return operation;
    }

    public String toString() {
        return "(" + operation.toString() + " " + expression + ")";
    }
}
