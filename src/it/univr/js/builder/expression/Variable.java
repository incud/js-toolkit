package it.univr.js.builder.expression;

/**
 * Representation of a variable, used in expressions and in DeclarationStatement
 */
public class Variable implements Expression {

    private final String identifier;

    public Variable(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifier() {
        return identifier;
    }

    @Override
    public String toString() {
        return getIdentifier();
    }
}
