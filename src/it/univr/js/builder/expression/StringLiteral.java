package it.univr.js.builder.expression;

public class StringLiteral extends Literal<String> {

    public StringLiteral(String value) {
        super(value);
    }
}
