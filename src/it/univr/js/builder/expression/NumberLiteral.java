package it.univr.js.builder.expression;

public class NumberLiteral extends Literal<Double> {

    public NumberLiteral(Double value) {
        super(value);
    }
}
