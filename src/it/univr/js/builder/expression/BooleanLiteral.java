package it.univr.js.builder.expression;

public class BooleanLiteral extends Literal<Boolean> {

    public BooleanLiteral(Boolean value) {
        super(value);
    }
}
