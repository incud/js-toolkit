package it.univr.js.builder.expression;

public enum UnaryOperation {

    NOT("!"), POS("+"), NEG("-");

    private final String operatorSyntax;

    UnaryOperation(String s) {
        this.operatorSyntax = s;
    }

    @Override public String toString() {
        return operatorSyntax;
    }
}
