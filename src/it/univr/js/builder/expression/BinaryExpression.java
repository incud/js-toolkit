package it.univr.js.builder.expression;

public class BinaryExpression implements Expression {

    private final Expression expr1, expr2;

    private final BinaryOperation operation;

    public BinaryExpression(Expression expr1, Expression expr2, BinaryOperation operation) {
        this.expr1 = expr1;
        this.expr2 = expr2;
        this.operation = operation;
    }

    public Expression getExpression1() {
        return expr1;
    }

    public Expression getExpression2() {
        return expr2;
    }

    public BinaryOperation getOperation() {
        return operation;
    }

    @Override
    public String toString() {
        return "(" + expr1 + " " + operation.toString() + " " + expr2 + ")";
    }
}
