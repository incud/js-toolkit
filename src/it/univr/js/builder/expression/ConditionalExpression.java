package it.univr.js.builder.expression;

public class ConditionalExpression implements Expression {

    private final Expression test, expressionTrue, expressionFalse;

    public ConditionalExpression(Expression test, Expression expressionTrue, Expression expressionFalse) {
        this.test = test;
        this.expressionTrue = expressionTrue;
        this.expressionFalse = expressionFalse;
    }

    public Expression getTest() {
        return test;
    }

    public Expression getExpressionTrue() {
        return expressionTrue;
    }

    public Expression getExpressionFalse() {
        return expressionFalse;
    }

    @Override
    public String toString() {
        return String.format("( %s ? %s : %s)", test, expressionTrue, expressionFalse);
    }
}
