package it.univr.js.builder.expression;

public class Literal<T> implements Expression {

    private final T value;

    public Literal(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "" + value;
    }
}
