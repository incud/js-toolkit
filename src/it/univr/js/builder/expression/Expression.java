package it.univr.js.builder.expression;

/**
 * This interface is used to recursively define the classes
 * - Variable
 * - UnaryExpression
 * - BinaryExpression
 * - Ternary expression (still todo)
 * - Number literal
 * - String literal (still todo)
 * - Boolean literal (still todo)
 * - Object literal (still todo)
 * - do I miss anything?
 */
public interface Expression {
}
