package it.univr.js.builder;

import java.util.LinkedList;

/**
 * A block is a sequence of statement, implemented as LinkedList
 * According to Java documentation, LinkedList is double linked cyclic list
 * so boh getFirst() and getLast() are O(1) operation in time
 */
public class Block extends LinkedList<Statement> {

    private Statement parent;

    public Statement getParent() {
        return parent;
    }

    public void setParent(Statement parent) {
        this.parent = parent;
    }

    @Override
    public boolean add(Statement statement) {
        // add to the list of statements
        boolean ok = super.add(statement);
        assert(ok);

        // automatically set parent of statement
        statement.setParent(this);
        return true;
    }

    public String getUniqueIdentifier() {
        return "BLOCK" + get(0).getUniqueNumber();
    }

    @Override
    public String toString() {
        if(getParent() == null) {
            return "ROOT BLOCK";
        } else {
            return "BLOCK of " + getParent() + " with " + size() + " elements";
        }
    }
}
