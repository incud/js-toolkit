package it.univr.js.builder.controlstructure;

import it.univr.js.builder.Block;
import it.univr.js.builder.Statement;
import it.univr.js.builder.expression.Expression;

public class DoWhileStatement extends SingleBlockControlStatement implements TrickyFirstInstruction {

    public DoWhileStatement(Block blockTrueCondition, Expression condition) {
        super(condition, blockTrueCondition);
    }

    @Override
    public String toString() {
        return "do ... while " + getCondition();
    }
}
