package it.univr.js.builder.controlstructure;

import it.univr.js.builder.Block;
import it.univr.js.builder.Statement;
import it.univr.js.builder.expression.Variable;

public class TryCatchStatement extends Statement implements TrickyLastInstruction {

    private final Block tryBlock;

    private final Block catchBlock;

    private final Variable catchVariable;

    public TryCatchStatement(Block tryBlock, Block catchBlock, Variable catchVariable) {
        this.tryBlock = tryBlock;
        this.catchBlock = catchBlock;
        this.catchVariable = catchVariable;
        this.tryBlock.setParent(this);
        this.catchBlock.setParent(this);
    }

    public Block getTryBlock() {
        return tryBlock;
    }

    public Block getCatchBlock() {
        return catchBlock;
    }

    public Variable getCatchVariable() {
        return catchVariable;
    }

    @Override
    public String toString() {
        return "try";
    }
}
