package it.univr.js.builder.controlstructure;

import it.univr.js.builder.Block;
import it.univr.js.builder.expression.Variable;

public class TryCatchFinallyStatement extends TryCatchStatement implements TrickyLastInstruction {

    private final Block finallyBlock;

    public TryCatchFinallyStatement(Block tryBlock, Block catchBlock, Variable catchVariable, Block finallyBlock) {
        super(tryBlock, catchBlock, catchVariable);
        this.finallyBlock = finallyBlock;
        this.finallyBlock.setParent(this);
    }

    public Block getFinallyBlock() {
        return finallyBlock;
    }
}
