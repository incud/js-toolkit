package it.univr.js.builder.controlstructure;

import it.univr.js.builder.Block;
import it.univr.js.builder.expression.Expression;

public class IfStatement extends SingleBlockControlStatement implements TrickyLastInstruction {

    public IfStatement(Expression condition, Block blockTrueCondition) {
        super(condition, blockTrueCondition);
    }

    @Override
    public String toString() {
        return "if " + getCondition();
    }
}
