package it.univr.js.builder.controlstructure;

import it.univr.js.builder.Block;
import it.univr.js.builder.expression.Expression;

public class IfElseStatement extends DoubleBlockControlStatement implements TrickyLastInstruction {

    public IfElseStatement(Expression condition, Block blockTrueCondition, Block blockConditionFalse) {
        super(condition, blockTrueCondition, blockConditionFalse);
    }

    @Override
    public String toString() {
        return "if " + getCondition();
    }
}
