package it.univr.js.builder.controlstructure;

import it.univr.js.builder.Block;
import it.univr.js.builder.expression.Expression;

public class WhileStatement extends SingleBlockControlStatement {

    public WhileStatement(Expression condition, Block blockTrueCondition) {
        super(condition, blockTrueCondition);
    }

    @Override
    public String toString() {
        return "while " + getCondition();
    }
}
