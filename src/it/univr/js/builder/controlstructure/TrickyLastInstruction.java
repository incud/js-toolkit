package it.univr.js.builder.controlstructure;

/**
 * If the statement is tagged as TrickyLastInstruction, it means that this statement
 * has blocks whose last statement will be connect with this statement's successor
 * Examples of this classes ase IfStatement, IfElseStatement, TryCatchStatement
 */
public interface TrickyLastInstruction {
}
