package it.univr.js.builder.controlstructure;

import it.univr.js.builder.Block;
import it.univr.js.builder.expression.Expression;

public class DoubleBlockControlStatement extends SingleBlockControlStatement {

    private final Block blockConditionFalse;

    public DoubleBlockControlStatement(Expression condition, Block blockTrueCondition, Block blockConditionFalse) {
        super(condition, blockTrueCondition);
        this.blockConditionFalse = blockConditionFalse;
        this.blockConditionFalse.setParent(this);
    }

    public Block getBlockFalseCondition() {
        return blockConditionFalse;
    }
}
