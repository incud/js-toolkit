package it.univr.js.builder.controlstructure;

import it.univr.js.builder.Block;
import it.univr.js.builder.Statement;
import it.univr.js.builder.expression.Expression;

public class SingleBlockControlStatement extends Statement {

    private final Block blockTrueCondition;

    private final Expression condition;

    public SingleBlockControlStatement(Expression condition, Block blockTrueCondition) {
        this.condition = condition;
        this.blockTrueCondition = blockTrueCondition;
        this.blockTrueCondition.setParent(this);
    }

    public Block getBlockTrueCondition() {
        return blockTrueCondition;
    }

    public Expression getCondition() {
        return condition;
    }
}
