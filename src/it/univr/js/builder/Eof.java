package it.univr.js.builder;

public class Eof extends Statement {

    @Override
    public String toString() { return "EOF"; }
}
