package it.univr.js.builder.abstractpoint;

public final class AbstractStatementPoint extends AbstractPoint {

    private final int line;

    private final int offset;

    public AbstractStatementPoint(int line, int offset) {
        this.line = line;
        this.offset = offset;
    }

    @Override
    public int getLine() {
        return line;
    }

    public int getOffset() {
        return offset;
    }

    public AbstractStatementPoint nextLine() {
        return new AbstractStatementPoint(line + 1, 0);
    }

    public AbstractStatementPoint nextOffset() {
        return new AbstractStatementPoint(line, offset + 1);
    }

    @Override
    public String toString() {
        return line + ":" + offset;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractStatementPoint that = (AbstractStatementPoint) o;

        if (line != that.line) return false;
        return offset == that.offset;
    }

    @Override
    public int hashCode() {
        int result = line;
        result = 31 * result + offset;
        return result;
    }
}
