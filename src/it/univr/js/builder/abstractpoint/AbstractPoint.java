package it.univr.js.builder.abstractpoint;

public abstract class AbstractPoint {

    public abstract int getLine();

}
