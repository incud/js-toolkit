package it.univr.js.builder.abstractpoint;

public final class AbstractShortcutPoint extends AbstractPoint {

    public enum Shortcut {
        NEXT, BRANCH_TRUE, BRANCH_FALSE, TRY, CATCH, FINALLY
    }

    private final int line;

    private final Shortcut shortcut;

    public AbstractShortcutPoint(int line, Shortcut label) {
        this.line = line;
        this.shortcut = label;
    }

    public AbstractShortcutPoint(AbstractPoint pt, Shortcut label) {
        this.line = pt.getLine();
        this.shortcut = label;
    }

    @Override
    public int getLine() {
        return line;
    }

    public Shortcut getShortcut() {
        return shortcut;
    }

    @Override
    public String toString() {
        return line + "@" + shortcut.toString();
    }

    public AbstractStatementPoint getFromPoint() {
        return new AbstractStatementPoint(line, 0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractShortcutPoint that = (AbstractShortcutPoint) o;

        if (line != that.line) return false;
        return shortcut == that.shortcut;
    }

    @Override
    public int hashCode() {
        int result = line;
        result = 31 * result + shortcut.hashCode();
        return result;
    }
}
