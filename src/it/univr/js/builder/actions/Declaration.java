package it.univr.js.builder.actions;

import it.univr.js.builder.expression.Expression;
import it.univr.js.builder.expression.Variable;

public class Declaration {

    private final DeclarationType type;

    private final Variable variable;

    private final Expression initializer;

    public Declaration(DeclarationType type, Variable variable, Expression initializer) {
        this.type = type;
        this.variable = variable;
        this.initializer = initializer;
    }

    public DeclarationType getType() {
        return type;
    }

    public Variable getVariable() {
        return variable;
    }

    public Expression getInitializer() {
        return initializer;
    }

    @Override
    public String toString() {
        if(initializer == null) {
            return "" + variable;
        } else {
            return variable + " = " + initializer;
        }
    }
}
