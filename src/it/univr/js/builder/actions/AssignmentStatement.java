package it.univr.js.builder.actions;

import it.univr.js.builder.Statement;
import it.univr.js.builder.expression.Expression;
import it.univr.js.builder.expression.Variable;

public class AssignmentStatement extends Statement implements Expression {

    private final Variable leftHandSide;

    private final Expression rightHandSide;

    public AssignmentStatement(Variable leftHandSide, Expression rightHandSide) {
        this.leftHandSide = leftHandSide;
        this.rightHandSide = rightHandSide;
    }

    public Variable getLeftHandSide() {
        return leftHandSide;
    }

    public Expression getRightHandSide() {
        return rightHandSide;
    }

    @Override
    public String toString() {
        return leftHandSide + " = " + rightHandSide;
    }
}
