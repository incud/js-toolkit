package it.univr.js.builder.actions;

import it.univr.js.builder.expression.BinaryOperation;
import it.univr.js.builder.expression.Expression;
import it.univr.js.builder.expression.Variable;

public class ComplexAssignmentStatement extends AssignmentStatement {

    private final BinaryOperation operation;

    public ComplexAssignmentStatement(Variable leftHandSide, Expression rightHandSide, BinaryOperation operation) {
        super(leftHandSide, rightHandSide);
        this.operation = operation;
    }

    public BinaryOperation getOperation() {
        return operation;
    }
}
