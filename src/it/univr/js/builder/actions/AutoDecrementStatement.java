package it.univr.js.builder.actions;

import it.univr.js.builder.Statement;
import it.univr.js.builder.expression.Expression;
import it.univr.js.builder.expression.Variable;

public class AutoDecrementStatement extends Statement implements Expression {

    private final Variable variable;

    public AutoDecrementStatement(Variable variable) {
        this.variable = variable;
    }

    public Variable getVariable() {
        return variable;
    }

    @Override
    public String toString() {
        return variable + "--";
    }
}
