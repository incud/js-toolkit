package it.univr.js.builder.actions;

import it.univr.js.builder.Statement;

public class EmptyStatement extends Statement {

    @Override
    public String toString() {
        return "<empty statement>";
    }
}
