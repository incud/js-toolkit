package it.univr.js.builder.actions;

import it.univr.js.builder.Statement;

import java.util.*;

public class DeclarationStatement extends Statement {

    private final List<Declaration> declarations;

    public DeclarationStatement(List<Declaration> decls) {
        if(decls.size() == 0) {
            throw new IllegalArgumentException("List of declarations must be non empty");
        }
        DeclarationType type = decls.get(0).getType();
        for(Declaration declaration : decls) {
            if(declaration.getType() != type)
                throw new IllegalArgumentException("Declaration must have only one type (first has " + type + " then another has " + declaration.getType());
        }

        declarations = Collections.unmodifiableList(decls);
    }

    public DeclarationStatement(Declaration... decls) {
        this(Arrays.asList(decls));
    }

    public List<Declaration> getDeclarations() {
        return Collections.unmodifiableList(declarations);
    }

    public String toString() {
        String declType = declarations.get(0).getType().toString().toLowerCase();

        String list = "", prefix = "";
        for(Declaration decl : declarations) {
            list += prefix + decl;
            prefix = ", ";
        }

        return declType + list + ";";
    }
}
