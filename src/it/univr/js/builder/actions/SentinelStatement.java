package it.univr.js.builder.actions;

import it.univr.js.builder.Statement;

/**
 * Created by incud on 29/10/17.
 */
public class SentinelStatement extends Statement {

    public SentinelStatement() {
    }

    @Override
    public String toString() {
        return "Sentinel";
    }
}
