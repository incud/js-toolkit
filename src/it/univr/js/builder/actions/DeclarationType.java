package it.univr.js.builder.actions;

public enum DeclarationType {
    VAR, LET, CONST
}
