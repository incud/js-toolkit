package it.univr.js.serializer;

import it.univr.js.builder.Block;
import it.univr.js.builder.Statement;
import it.univr.js.builder.controlstructure.DoubleBlockControlStatement;
import it.univr.js.builder.controlstructure.SingleBlockControlStatement;
import it.univr.js.builder.controlstructure.TryCatchFinallyStatement;
import it.univr.js.builder.controlstructure.TryCatchStatement;
import it.univr.js.graph.Graph;

public class SerializedAstGraph extends Graph {

    private final SerializedAst ast;

    public SerializedAstGraph(SerializedAst ast) {
        this.ast = ast;
        processBlock(ast.getRoot());
    }

    private void connect(Block block) {
        if(block != ast.getRoot()) {
            addEdge(block.getParent().getUniqueIdentifier(), block.getUniqueIdentifier());
        }
    }

    private void connect(Statement a, Statement b) {
        addEdge(a.getUniqueIdentifier(), b.getUniqueIdentifier());
    }

    private void connect(Block a, Statement b) {
        addEdge(a.getUniqueIdentifier(), b.getUniqueIdentifier());
    }

    private void processBlock(Block block) {

        addNode(block.getUniqueIdentifier(), "BLOCK");
        connect(block);

        Statement previous = null;
        for(Statement stmt : block) {
            // add statement
            processStatement(stmt);
            // connect with the one before
            if(previous == null) {
                connect(block, stmt);
            } else {
                connect(previous, stmt);
            }
            // increment pointer
            previous = stmt;
        }
    }

    private void processStatement(Statement stmt) {
        addNode(stmt.getUniqueIdentifier(), stmt.toString());

        if(stmt instanceof DoubleBlockControlStatement) {
            DoubleBlockControlStatement doubleStmt = (DoubleBlockControlStatement)stmt;
            processBlock(doubleStmt.getBlockTrueCondition());
            processBlock(doubleStmt.getBlockFalseCondition());

        } else if(stmt instanceof SingleBlockControlStatement) {
            SingleBlockControlStatement singleStmt = (SingleBlockControlStatement)stmt;
            processBlock(singleStmt.getBlockTrueCondition());

        } else if(stmt instanceof TryCatchFinallyStatement) {
            TryCatchFinallyStatement tcfStatement = (TryCatchFinallyStatement)stmt;
            processBlock(tcfStatement.getTryBlock());
            processBlock(tcfStatement.getCatchBlock());
            processBlock(tcfStatement.getFinallyBlock());

        } else if(stmt instanceof TryCatchStatement) {
            TryCatchStatement tcStatement = (TryCatchStatement)stmt;
            processBlock(tcStatement.getTryBlock());
            processBlock(tcStatement.getCatchBlock());
        }
    }
}
