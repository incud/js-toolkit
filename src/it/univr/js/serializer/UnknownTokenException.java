package it.univr.js.serializer;

import org.mozilla.javascript.Token;
import org.mozilla.javascript.ast.AstNode;

public class UnknownTokenException extends RuntimeException {

    public UnknownTokenException(AstNode node) {
        super(String.format("Unknown node %s (Token.%s) (source: %s)",
                node.shortName(),
                Token.typeToName(node.getType()),
                node.toSource()));
    }
}
