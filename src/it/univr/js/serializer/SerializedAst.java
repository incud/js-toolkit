package it.univr.js.serializer;

import it.univr.js.builder.*;
import it.univr.js.builder.Block;
import it.univr.js.builder.actions.*;
import it.univr.js.builder.actions.EmptyStatement;
import it.univr.js.builder.controlstructure.*;
import it.univr.js.builder.controlstructure.IfElseStatement;
import it.univr.js.builder.controlstructure.IfStatement;
import it.univr.js.builder.expression.*;
import it.univr.js.builder.expression.ConditionalExpression;
import it.univr.js.builder.expression.NumberLiteral;
import it.univr.js.builder.expression.StringLiteral;
import it.univr.js.builder.expression.UnaryExpression;
import it.univr.js.parser.RhinoAst;
import org.mozilla.javascript.Node;
import org.mozilla.javascript.Token;
import org.mozilla.javascript.ast.*;

import java.util.LinkedList;
import java.util.List;

/**
 * the SerializedAst class gets a Rhino abstract syntax tree
 * (in form of list of org.mozilla.javascript.ast.AstNode)
 * and outputs the abstract syntax tree in our representation
 */
public class SerializedAst {

    /**
     * Root of the AST
     */
    private final Block statementList;

    /**
     * Input of the operation: Rhino's AST
     * @param rhinoAst
     */
    public SerializedAst(RhinoAst rhinoAst) {
        statementList = processBlock(rhinoAst);
        statementList.add(new Eof());
    }

    public Block getRoot() {
        return statementList;
    }

    /**
     * Process a list of Rhino AstNodes and outputs a Block
     * @param list list of Rhino nodes
     * @return a Block
     */
    private Block processBlock(Iterable<AstNode> list) {
        Block currentList = new Block();

        for(AstNode node : list) {
            Statement statement = processStatement(node);
            currentList.add(statement);
        }

        // special case: empty list
        if(currentList.isEmpty()) {
            currentList.add(new EmptyStatement());
            return currentList;
        }

        // if first element is problematic
        if(currentList.getFirst() instanceof TrickyFirstInstruction) {
            currentList.addFirst(new SentinelStatement());
        }

        // if last label is problematic
        if(currentList.getLast() instanceof TrickyLastInstruction) {
            currentList.add(new SentinelStatement());
        }

        return currentList;
    }

    /**
     * Process the current node and return the corresponding statement
     * @param node node
     * @return statement
     */
    private Statement processStatement(AstNode node) {
        switch (node.getType()) {
            case Token.VAR:
            case Token.CONST:
            case Token.LET: {
                if(node instanceof VariableDeclaration) {
                    return processVariableDeclaration((VariableDeclaration)node);
                } else if (node instanceof VariableInitializer) {
                    return processVariableInitializer((VariableInitializer)node);
                } else {
                    throw new AssertionError("This must be VariableDeclaration or VariableInitializer");
                }
            }
            case Token.WHILE:
                return processWhile((WhileLoop)node);
            case Token.DO:
                return processDoWhile((DoLoop)node);
            case Token.IF:
                return processIf((org.mozilla.javascript.ast.IfStatement)node);
            case Token.TRY:
                return processTry((org.mozilla.javascript.ast.TryStatement)node);
            case Token.EXPR_RESULT:
                return processExpressionStatement((ExpressionStatement)node);
            case Token.ASSIGN:
                return processAssignment((Assignment)node);
            case Token.ASSIGN_ADD:
                return processComplexAssignment((Assignment)node, BinaryOperation.ADD);
            case Token.ASSIGN_SUB:
                return processComplexAssignment((Assignment)node, BinaryOperation.SUB);
            case Token.ASSIGN_MUL:
                return processComplexAssignment((Assignment)node, BinaryOperation.MUL);
            case Token.ASSIGN_DIV:
                return processComplexAssignment((Assignment)node, BinaryOperation.DIV);
            case Token.ASSIGN_MOD:
                return processComplexAssignment((Assignment)node, BinaryOperation.MOD);
            case Token.ASSIGN_BITAND:
                return processComplexAssignment((Assignment)node, BinaryOperation.AND);
            case Token.ASSIGN_BITOR:
                return processComplexAssignment((Assignment)node, BinaryOperation.OR);
            case Token.ASSIGN_BITXOR:
                return processComplexAssignment((Assignment)node, BinaryOperation.XOR);
            case Token.ASSIGN_LSH:
                return processComplexAssignment((Assignment)node, BinaryOperation.LEFT_SHIFT);
            case Token.ASSIGN_RSH:
                return processComplexAssignment((Assignment)node, BinaryOperation.RIGHT_SHIFT);
            case Token.ASSIGN_URSH:
                return processComplexAssignment((Assignment)node, BinaryOperation.UNSIGNED_RIGHT_SHIFT);
            case Token.INC:
                return processAutoIncrement((org.mozilla.javascript.ast.UnaryExpression)node);
            case Token.DEC:
                return processAutoDecrement((org.mozilla.javascript.ast.UnaryExpression)node);
            default:
                throw new UnknownTokenException(node);
        }
    }

    private DeclarationStatement processVariableDeclaration(VariableDeclaration rhinoVarDecl) {
        List<Declaration> declarations = new LinkedList<>();
        // for each variable initialization in this declaration
        for(VariableInitializer variableInitializer : rhinoVarDecl.getVariables()) {
            declarations.add(createDeclaration(variableInitializer));
        }
        // return declaration statement
        return new DeclarationStatement(declarations);
    }

    private DeclarationStatement processVariableInitializer(VariableInitializer rhinoValInit) {
        return new DeclarationStatement(createDeclaration(rhinoValInit));
    }

    private Declaration createDeclaration(VariableInitializer rhinoValInit) {
        // get the identifier
        Variable var = processVariable((Name)rhinoValInit.getTarget());
        // get the expression
        Expression expression = null;
        if(rhinoValInit.getInitializer() != null) {
            expression = processExpression(rhinoValInit.getInitializer());
        }
        // get the type
        int token = rhinoValInit.getType();
        DeclarationType type = (token == Token.VAR) ? DeclarationType.VAR : (token == Token.LET ? DeclarationType.LET : DeclarationType.CONST);
        // return declaration
        return new Declaration(type, var, expression);
    }

    private WhileStatement processWhile(WhileLoop whileLoop) {
        // create while condition
        Expression condition = processExpression(whileLoop.getCondition());
        // create while body
        Block body = processBlock(whileLoop.getBody());
        // return while
        return new WhileStatement(condition, body);
    }

    private DoWhileStatement processDoWhile(DoLoop doWhileLoop) {
        // create while condition
        Expression condition = processExpression(doWhileLoop.getCondition());
        // create while body
        Block body = processBlock(doWhileLoop.getBody());
        // return while
        return new DoWhileStatement(body, condition);
    }

    private Statement processIf(org.mozilla.javascript.ast.IfStatement ifStatement) {
        // create condition
        Expression condition = processExpression(ifStatement.getCondition());
        // get true block
        Block bodyTrue = processBlock(ifStatement.getThenPart());
        // get else block if exists
        if(ifStatement.getElsePart() != null) {
            Block bodyFalse = processBlock(ifStatement.getElsePart());
            return new IfElseStatement(condition, bodyTrue, bodyFalse);
        } else {
            return new IfStatement(condition, bodyTrue);
        }
    }

    private Statement processTry(org.mozilla.javascript.ast.TryStatement tryStatement) {
        // get try block
        Block tryBlock = processBlock(tryStatement.getTryBlock());
        // get the only one catch clause
        assert (tryStatement.getCatchClauses().size() == 1);
        CatchClause catchClause = tryStatement.getCatchClauses().get(0);
        // get catch block & variable
        Block catchBlock = processBlock(catchClause.getBody());
        Variable catchVariable = processVariable(catchClause.getVarName());

        if(tryStatement.getFinallyBlock() == null) {
            return new TryCatchStatement(tryBlock, catchBlock, catchVariable);
        } else {
            Block finallyBlock = processBlock(tryStatement.getFinallyBlock());
            return new TryCatchFinallyStatement(tryBlock, catchBlock, catchVariable, finallyBlock);
        }
    }

    private Block processBlock(AstNode node) {
        if(node instanceof Scope) {
            // whole block, with curly brackets
            Scope scope = (Scope)node;
            return processBlock(scope.getStatements());

        } if(node instanceof org.mozilla.javascript.ast.Block) {
            // whole block, with curly brackets
            org.mozilla.javascript.ast.Block block = (org.mozilla.javascript.ast.Block)node;
            return processBlock(SerializedAst.fromNodes(block));

        } else {
            // only a single instruction
            List<AstNode> nodes = new LinkedList<>();
            nodes.add(node);
            return processBlock(nodes);
        }
    }

    private Statement processExpressionStatement(ExpressionStatement expr) {
        return processStatement(expr.getExpression());
    }

    private AssignmentStatement processAssignment(Assignment a) {
        Variable left = processVariable((Name)a.getLeft());
        Expression right = processExpression(a.getRight());
        return new AssignmentStatement(left, right);
    }

    private ComplexAssignmentStatement processComplexAssignment(Assignment a, BinaryOperation op) {
        Variable left = processVariable((Name)a.getLeft());
        Expression right = processExpression(a.getRight());
        return new ComplexAssignmentStatement(left, right, op);
    }

    private AutoIncrementStatement processAutoIncrement(org.mozilla.javascript.ast.UnaryExpression e) {
        Variable variable = processVariable((Name)e.getOperand());
        return new AutoIncrementStatement(variable);
    }

    private AutoDecrementStatement processAutoDecrement(org.mozilla.javascript.ast.UnaryExpression e) {
        Variable variable = processVariable((Name)e.getOperand());
        return new AutoDecrementStatement(variable);
    }

    private Expression processExpression(AstNode node) {
        switch (node.getType()) {
            case Token.NUMBER:
                return processNumberLiteral((org.mozilla.javascript.ast.NumberLiteral)node);
            case Token.STRING:
                return processStringLiteral((org.mozilla.javascript.ast.StringLiteral)node);
            case Token.TRUE:
                return new BooleanLiteral(true);
            case Token.FALSE:
                return new BooleanLiteral(false);
            case Token.NAME:
                return processVariable((Name)node);
            case Token.NOT:
                return processPrefixExpression((org.mozilla.javascript.ast.UnaryExpression)node, UnaryOperation.NOT);
            case Token.NEG:
                return processPrefixExpression((org.mozilla.javascript.ast.UnaryExpression)node, UnaryOperation.NEG);
            case Token.POS:
                return processPrefixExpression((org.mozilla.javascript.ast.UnaryExpression)node, UnaryOperation.POS);
            case Token.ADD:
                return processInfixExpression((InfixExpression)node, BinaryOperation.ADD);
            case Token.SUB:
                return processInfixExpression((InfixExpression)node, BinaryOperation.SUB);
            case Token.MUL:
                return processInfixExpression((InfixExpression)node, BinaryOperation.MUL);
            case Token.DIV:
                return processInfixExpression((InfixExpression)node, BinaryOperation.DIV);
            case Token.MOD:
                return processInfixExpression((InfixExpression)node, BinaryOperation.MOD);
            case Token.AND:
                return processInfixExpression((InfixExpression)node, BinaryOperation.AND);
            case Token.OR:
                return processInfixExpression((InfixExpression)node, BinaryOperation.OR);
            case Token.BITXOR:
                return processInfixExpression((InfixExpression)node, BinaryOperation.XOR);
            case Token.LSH:
                return processInfixExpression((InfixExpression)node, BinaryOperation.LEFT_SHIFT);
            case Token.RSH:
                return processInfixExpression((InfixExpression)node, BinaryOperation.RIGHT_SHIFT);
            case Token.URSH:
                return processInfixExpression((InfixExpression)node, BinaryOperation.UNSIGNED_RIGHT_SHIFT);
            case Token.EQ:
                return processInfixExpression((InfixExpression)node, BinaryOperation.EQUALS);
            case Token.NE:
                return processInfixExpression((InfixExpression)node, BinaryOperation.NOT_EQUALS);
            case Token.LT:
                return processInfixExpression((InfixExpression)node, BinaryOperation.LESS);
            case Token.LE:
                return processInfixExpression((InfixExpression)node, BinaryOperation.LESS_EQUALS);
            case Token.GT:
                return processInfixExpression((InfixExpression)node, BinaryOperation.GREATER);
            case Token.GE:
                return processInfixExpression((InfixExpression)node, BinaryOperation.GREATER_EQUALS);
            case Token.HOOK:
                return processConditionalExpression((org.mozilla.javascript.ast.ConditionalExpression)node);
            case Token.EXPR_RESULT:
                return processExpression(((ExpressionStatement)node).getExpression());
            case Token.LP:
                return processExpression(((ParenthesizedExpression)node).getExpression());
            case Token.ASSIGN:
                return processAssignment((Assignment) node);
            case Token.ASSIGN_ADD:
                return processComplexAssignment((Assignment)node, BinaryOperation.ADD);
            case Token.ASSIGN_SUB:
                return processComplexAssignment((Assignment)node, BinaryOperation.SUB);
            case Token.ASSIGN_MUL:
                return processComplexAssignment((Assignment)node, BinaryOperation.MUL);
            case Token.ASSIGN_DIV:
                return processComplexAssignment((Assignment)node, BinaryOperation.DIV);
            case Token.ASSIGN_MOD:
                return processComplexAssignment((Assignment)node, BinaryOperation.MOD);
            case Token.ASSIGN_BITAND:
                return processComplexAssignment((Assignment)node, BinaryOperation.AND);
            case Token.ASSIGN_BITOR:
                return processComplexAssignment((Assignment)node, BinaryOperation.OR);
            case Token.ASSIGN_BITXOR:
                return processComplexAssignment((Assignment)node, BinaryOperation.XOR);
            case Token.ASSIGN_LSH:
                return processComplexAssignment((Assignment)node, BinaryOperation.LEFT_SHIFT);
            case Token.ASSIGN_RSH:
                return processComplexAssignment((Assignment)node, BinaryOperation.RIGHT_SHIFT);
            case Token.ASSIGN_URSH:
                return processComplexAssignment((Assignment)node, BinaryOperation.UNSIGNED_RIGHT_SHIFT);
            case Token.INC:
                return processAutoIncrement((org.mozilla.javascript.ast.UnaryExpression)node);
            case Token.DEC:
                return processAutoDecrement((org.mozilla.javascript.ast.UnaryExpression)node);
            default:
                throw new UnknownTokenException(node);
        }
    }

    private NumberLiteral processNumberLiteral(org.mozilla.javascript.ast.NumberLiteral number) {
        double value = Double.parseDouble(number.getValue());
        return new NumberLiteral(value);
    }

    private StringLiteral processStringLiteral(org.mozilla.javascript.ast.StringLiteral string) {
        String value = string.getValue();
        return new StringLiteral(value);
    }

    private Variable processVariable(Name name) {
        return new Variable(name.getIdentifier());
    }

    private UnaryExpression processPrefixExpression(org.mozilla.javascript.ast.UnaryExpression unaryExpression, UnaryOperation op) {
        Expression e = processExpression(unaryExpression.getOperand());
        return new UnaryExpression(e, op);
    }

    private BinaryExpression processInfixExpression(InfixExpression e, BinaryOperation op) {
        Expression left = processExpression(e.getLeft());
        Expression right = processExpression(e.getRight());
        return new BinaryExpression(left, right, op);
    }

    private ConditionalExpression processConditionalExpression(org.mozilla.javascript.ast.ConditionalExpression ce) {
        Expression test = processExpression(ce.getTestExpression());
        Expression trueExpression = processExpression(ce.getTrueExpression());
        Expression falseExpression = processExpression(ce.getFalseExpression());
        return new ConditionalExpression(test, trueExpression, falseExpression);
    }

    private static Iterable<AstNode> fromNodes(Iterable<Node> nodes) {
        List<AstNode> astNodes = new LinkedList<>();
        for(Node n : nodes) {
            astNodes.add((AstNode)n);
        }
        return astNodes;
    }
}
