package it.univr.js.labeling;

import it.univr.js.builder.abstractpoint.AbstractPoint;
import it.univr.js.builder.Statement;
import it.univr.js.builder.abstractpoint.AbstractShortcutPoint;
import it.univr.js.builder.abstractpoint.AbstractStatementPoint;
import it.univr.js.graph.Graph;
import it.univr.js.serializer.SerializedAst;

import java.awt.*;
import java.util.Map;

public class LabelizedAstGraph extends Graph {

    private final Map<AbstractPoint, Statement> cfgEdge;

    public LabelizedAstGraph(SerializedAst ast, Map<AbstractPoint, Statement> cfgEdge) {
        this.cfgEdge = cfgEdge;
        addNodes();
        addEdges();
    }

    private void addNode(Statement stmt) {
        addNode(stmt.getUniqueIdentifier(), String.format("[%s] ", stmt.getAbstractPoint()) + stmt.toString());
    }

    private void addEdge(Statement a, Statement b, AbstractPoint ap) {
        addEdge(a.getUniqueIdentifier(), b.getUniqueIdentifier(), Color.RED, "" + ap);
    }

    private void addNodes() {
        for(Map.Entry<AbstractPoint, Statement> entry : cfgEdge.entrySet()) {
            Statement stmt = entry.getValue();
            addNode(stmt);
        }
    }

    private void addEdges() {
        for(Map.Entry<AbstractPoint, Statement> entry : cfgEdge.entrySet()) {

            AbstractPoint ap = entry.getKey();
            Statement stmt = entry.getValue();


            if(ap instanceof AbstractStatementPoint) {
                addStatementEdge(stmt);
            } else if(ap instanceof AbstractShortcutPoint) {
                addShortcutEdge((AbstractShortcutPoint)ap, stmt);
            }
        }
    }

    private void addStatementEdge(Statement stmt) {
        // Graphstream has problem with self loops
        // addEdge(stmt, stmt, stmt.getAbstractPoint();
    }

    private void addShortcutEdge(AbstractShortcutPoint toPoint, Statement to) {
        AbstractPoint fromPoint = toPoint.getFromPoint();
        Statement from = cfgEdge.get(fromPoint);
        addEdge(from, to, toPoint);
    }

}
