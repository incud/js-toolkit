package it.univr.js.labeling;

import it.univr.js.builder.Statement;

/**
 * Created by incud on 29/10/17.
 */
public class UnknownStatementException extends RuntimeException {

    public UnknownStatementException(Statement stmt) {
        super(String.format("Unknown statement: %s (%s)", stmt, stmt.getClass().getCanonicalName()));
    }
}
