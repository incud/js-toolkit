package it.univr.js.labeling;

import it.univr.js.builder.Block;
import it.univr.js.builder.Eof;
import it.univr.js.builder.Statement;
import it.univr.js.builder.abstractpoint.AbstractPoint;
import it.univr.js.builder.abstractpoint.AbstractShortcutPoint;
import it.univr.js.builder.abstractpoint.AbstractStatementPoint;
import it.univr.js.builder.controlstructure.*;
import it.univr.js.serializer.SerializedAst;
import it.univr.js.builder.abstractpoint.AbstractShortcutPoint.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Labelizer {

    private final Map<AbstractPoint, Statement> cfgEdges = new HashMap<>();

    public Labelizer(SerializedAst ast) {
        processBlock(ast.getRoot(), new AbstractStatementPoint(0,0));
    }

    public Map<AbstractPoint, Statement> getCfgEdges() {
        return cfgEdges;
    }

    private AbstractStatementPoint processBlock(Block block, AbstractStatementPoint lastUsedPoint) {

        System.out.println(block);
        Iterator<Statement> blockIt = block.iterator();

        // maintains three pointer: the instruction before the current, the current, and the successor of the current
        Statement previous = block.getParent();
        Statement current = blockIt.next();
        Statement next = blockIt.hasNext() ? blockIt.next() : getSuccessorOf(block.getParent());

        // empty block
        if(current == null) {
            return lastUsedPoint;
        }

        // foreach statement from the 1st to the last-2
        AbstractStatementPoint point = lastUsedPoint;
        while(blockIt.hasNext()) {

            // assign label to statement
            point = processStatement(previous, current, next, point);

            // increment pointers
            previous = current;
            current = next;
            next = blockIt.next();
        }

        // last but one statement
        point = processStatement(previous, current, next, point);

        // increment pointers
        previous = current;
        current = next;
        next = getSuccessorOf(block.getParent());

        // last statement
        point = processStatement(previous, current, next, point);

        // return last used point
        return point;
    }

    private AbstractStatementPoint processStatement(Statement previous, Statement current, Statement next, AbstractStatementPoint lastUsedPoint) {

        // add current statement
        AbstractStatementPoint point = lastUsedPoint.nextLine();
        current.setAbstractPoint(point);
        cfgEdges.put(point, current);

        // connect to the previous one (if exists and if it's not a do-while statement)
        if(previous != null && !(current instanceof TrickyFirstInstruction) && !(previous instanceof TrickyLastInstruction)) {
            AbstractShortcutPoint nextShortcut = new AbstractShortcutPoint(previous.getAbstractPoint(), Shortcut.NEXT);
            cfgEdges.put(nextShortcut, current);
        }

        // add special blocks
        if(current instanceof Eof) {
            return point; // if EOF, stop here
        } else if(current instanceof IfElseStatement) {
            point = processIfElseStatement((IfElseStatement)current, next, point);
        } else if(current instanceof IfStatement) {
            point = processIfStatement((IfStatement)current, next, point);
        } else if(current instanceof WhileStatement) {
            point = processWhileStatement((WhileStatement)current, point);
        } else if(current instanceof DoWhileStatement) {
            point = processDoWhileStatement(previous, (DoWhileStatement)current, point);
        } else if(current instanceof TryCatchFinallyStatement) {
            point = processTryCatchFinallyStatement((TryCatchFinallyStatement)current, next, point);
        } else if(current instanceof TryCatchStatement) {
            point = processTryCatchStatement((TryCatchStatement)current, next, point);
        }

        return point;
    }

    private AbstractStatementPoint processIfElseStatement(IfElseStatement current, Statement next, AbstractStatementPoint lastUsedPoint) {
        AbstractStatementPoint point = lastUsedPoint;
        point = processBlock(current.getBlockTrueCondition(), point);
        point = processBlock(current.getBlockFalseCondition(), point);

        // connect first of both block with the current statement
        Statement firstOfTrueBlock = current.getBlockTrueCondition().getFirst();
        Statement firstOfFalseBlock = current.getBlockFalseCondition().getFirst();
        AbstractShortcutPoint shortcutStartTrueBlock = new AbstractShortcutPoint(current.getAbstractPoint(), Shortcut.BRANCH_TRUE);
        AbstractShortcutPoint shortcutStartFalseBlock = new AbstractShortcutPoint(current.getAbstractPoint(), Shortcut.BRANCH_FALSE);
        cfgEdges.put(shortcutStartTrueBlock, firstOfTrueBlock);
        cfgEdges.put(shortcutStartFalseBlock, firstOfFalseBlock);

        // connect last of both block with the next statement
        Statement lastOfTrueBlock = current.getBlockTrueCondition().getLast();
        Statement lastOfFalseBlock = current.getBlockFalseCondition().getLast();
        AbstractShortcutPoint shortcutTrueBlock = new AbstractShortcutPoint(lastOfTrueBlock.getAbstractPoint(), Shortcut.NEXT);
        AbstractShortcutPoint shortcutFalseBlock = new AbstractShortcutPoint(lastOfFalseBlock.getAbstractPoint(), Shortcut.NEXT);
        cfgEdges.put(shortcutTrueBlock, next);
        cfgEdges.put(shortcutFalseBlock, next);

        // return last used point
        return point;
    }

    private AbstractStatementPoint processIfStatement(IfStatement current, Statement next, AbstractStatementPoint lastUsedPoint) {
        AbstractStatementPoint point = lastUsedPoint;
        point = processBlock(current.getBlockTrueCondition(), point);

        // connect first stmt of the block with the current statement
        Statement firstOfTrueBlock = current.getBlockTrueCondition().getFirst();
        AbstractShortcutPoint shortcutStartTrueBlock = new AbstractShortcutPoint(current.getAbstractPoint(), Shortcut.BRANCH_TRUE);
        cfgEdges.put(shortcutStartTrueBlock, firstOfTrueBlock);

        // connect last of both block with the next statement
        Statement lastOfTrueBlock = current.getBlockTrueCondition().getLast();
        AbstractShortcutPoint shortcutTrueBlock = new AbstractShortcutPoint(lastOfTrueBlock.getAbstractPoint(), Shortcut.NEXT);
        cfgEdges.put(shortcutTrueBlock, next);

        // return last used point
        return point;
    }

    private AbstractStatementPoint processWhileStatement(WhileStatement current, AbstractStatementPoint lastUsedPoint) {
        AbstractStatementPoint point = lastUsedPoint;
        point = processBlock(current.getBlockTrueCondition(), point);

        // WHILE -> first of the block
        Statement firstOfTrueBlock = current.getBlockTrueCondition().getFirst();
        AbstractShortcutPoint shortcutStartTrueBlock = new AbstractShortcutPoint(current.getAbstractPoint(), Shortcut.BRANCH_TRUE);
        cfgEdges.put(shortcutStartTrueBlock, firstOfTrueBlock);

        // last of the block -> WHILE
        Statement lastOfTrueBlock = current.getBlockTrueCondition().getLast();
        AbstractShortcutPoint shortcutTrueBlock = new AbstractShortcutPoint(lastOfTrueBlock.getAbstractPoint(), Shortcut.NEXT);
        cfgEdges.put(shortcutTrueBlock, current);

        return point;
    }

    private AbstractStatementPoint processDoWhileStatement(Statement previous, DoWhileStatement current, AbstractStatementPoint lastUsedPoint) {
        AbstractStatementPoint point = lastUsedPoint;
        point = processBlock(current.getBlockTrueCondition(), point);

        Statement firstOfTrueBlock = current.getBlockTrueCondition().getFirst();
        Statement lastOfTrueBlock = current.getBlockTrueCondition().getLast();

        // previous -> first of the block
        AbstractShortcutPoint previousShortcut = new AbstractShortcutPoint(previous.getAbstractPoint(), Shortcut.NEXT);
        cfgEdges.put(previousShortcut, firstOfTrueBlock);

        // DO -> first of the block
        AbstractShortcutPoint doShortcut = new AbstractShortcutPoint(current.getAbstractPoint(), Shortcut.BRANCH_TRUE);
        cfgEdges.put(doShortcut, firstOfTrueBlock);

        // last of the block -> WHILE
        AbstractShortcutPoint goUpShortcut = new AbstractShortcutPoint(lastOfTrueBlock.getAbstractPoint(), Shortcut.NEXT);
        cfgEdges.put(goUpShortcut, current);

        return point;
    }

    private AbstractStatementPoint processTryCatchStatement(TryCatchStatement current, Statement next, AbstractStatementPoint lastUsedPoint) {
        AbstractStatementPoint point = lastUsedPoint;
        point = processBlock(current.getTryBlock(), point);
        point = processBlock(current.getCatchBlock(), point);

        // connect first of both block with the current statement
        Statement firstOfTryBlock = current.getTryBlock().getFirst();
        Statement firstOfCatchBlock = current.getCatchBlock().getFirst();
        AbstractShortcutPoint tryShortcut = new AbstractShortcutPoint(current.getAbstractPoint(), Shortcut.TRY);
        AbstractShortcutPoint catchShortcut = new AbstractShortcutPoint(current.getAbstractPoint(), Shortcut.CATCH);
        cfgEdges.put(tryShortcut, firstOfTryBlock);
        cfgEdges.put(catchShortcut, firstOfCatchBlock);

        // connect last of both block with the next statement
        Statement lastOfTryBlock = current.getTryBlock().getLast();
        Statement lastOfCatchBlock = current.getCatchBlock().getLast();
        AbstractShortcutPoint lastTryShortcut = new AbstractShortcutPoint(lastOfTryBlock.getAbstractPoint(), Shortcut.NEXT);
        AbstractShortcutPoint lastCatchShortcut = new AbstractShortcutPoint(lastOfCatchBlock.getAbstractPoint(), Shortcut.NEXT);
        cfgEdges.put(lastTryShortcut, next);
        cfgEdges.put(lastCatchShortcut, next);

        // return last used point
        return point;
    }

    private AbstractStatementPoint processTryCatchFinallyStatement(TryCatchFinallyStatement current, Statement next, AbstractStatementPoint lastUsedPoint) {
        AbstractStatementPoint point = lastUsedPoint;
        point = processBlock(current.getTryBlock(), point);
        point = processBlock(current.getCatchBlock(), point);
        point = processBlock(current.getFinallyBlock(), point);

        // connect first of both block with the current statement
        Statement firstOfTryBlock = current.getTryBlock().getFirst();
        Statement firstOfCatchBlock = current.getCatchBlock().getFirst();
        Statement firstOfFinallyBlock = current.getFinallyBlock().getFirst();
        AbstractShortcutPoint tryShortcut = new AbstractShortcutPoint(current.getAbstractPoint(), Shortcut.TRY);
        AbstractShortcutPoint catchShortcut = new AbstractShortcutPoint(current.getAbstractPoint(), Shortcut.CATCH);
        AbstractShortcutPoint finallyShortcut = new AbstractShortcutPoint(current.getAbstractPoint(), Shortcut.FINALLY);
        cfgEdges.put(tryShortcut, firstOfTryBlock);
        cfgEdges.put(catchShortcut, firstOfCatchBlock);
        cfgEdges.put(finallyShortcut, firstOfFinallyBlock);

        // connect last of both block with the next statement
        Statement lastOfTryBlock = current.getTryBlock().getLast();
        Statement lastOfCatchBlock = current.getCatchBlock().getLast();
        Statement lastOfFinallyBlock = current.getFinallyBlock().getLast();
        AbstractShortcutPoint lastTryShortcut = new AbstractShortcutPoint(lastOfTryBlock.getAbstractPoint(), Shortcut.NEXT);
        AbstractShortcutPoint lastCatchShortcut = new AbstractShortcutPoint(lastOfCatchBlock.getAbstractPoint(), Shortcut.NEXT);
        AbstractShortcutPoint lastFinallyShortcut = new AbstractShortcutPoint(lastOfFinallyBlock.getAbstractPoint(), Shortcut.NEXT);
        cfgEdges.put(lastTryShortcut, next);
        cfgEdges.put(lastCatchShortcut, next);
        cfgEdges.put(lastFinallyShortcut, next);

        // return last used point
        return point;
    }

    /**
     * Get the statement stmt container's block, traverse the block until you find the caller's statement,
     * then return the next statement of the block. If it's the last statement, get the parent's successor recursively
     * @param stmt statement
     * @return its successor
     */
    private Statement getSuccessorOf(Statement stmt) {

        // eof special case
        if(stmt == null) {
            return null;
        }

        // parent block
        Block parent = stmt.getParent();

        // traverse list until you find the
        for(Iterator<Statement> it = parent.iterator(); it.hasNext(); ) {
            Statement current = it.next();
            if(current == stmt) { // found statement!
                if(it.hasNext()) { // normal situation
                    return it.next();
                } else { // the block just ended
                    return getSuccessorOf(parent.getParent());
                }
            }
        }

        throw new AssertionError("It's impossible you can't find the successor of the parent");
    }

}
